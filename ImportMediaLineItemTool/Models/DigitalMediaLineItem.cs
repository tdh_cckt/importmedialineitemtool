﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ImportMediaLineItemTool.Models
{
    public class DigitalMediaLineItem
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public bool IsDeleted { get; set; }
        public string Name { get; set; }
        public Guid MediaPlanId { get; set; }
        public string MediaTypeId { get; set; }
        public string SubMediaTypeId { get; set; }
        public Guid SupplierProductId { get; set; }
        public Guid PublisherId { get; set; }
        public Guid SupplierId { get; set; }
        public string Format { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string LandingPage { get; set; }
        public string AdServingType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Guid BuyingModel { get; set; }
        public int Volume { get; set; }
        public Guid SpotTypeId { get; set; }
        public Decimal BaseRate { get; set; }
        public Decimal BaseCost { get; set; }
        public Decimal ClientCostDiscount { get; set; }
        public Decimal ClientCostLoading { get; set; }
        public Decimal ClientCostNetCost { get; set; }
        public Decimal ClientCostOtherCost { get; set; }
        public Decimal ClientCostAgencyFee { get; set; }
        public Decimal ClientCostTax1 { get; set; }
        public Decimal ClientCostTax2 { get; set; }
        public Decimal ClientCostTotal { get; set; }
        public Decimal SupplierCostDiscount { get; set; }
        public Decimal SupplierCostLoading { get; set; }
        public Decimal SupplierCostNetCost { get; set; }
        public Decimal SupplierCostOtherCost { get; set; }
        public Decimal SupplierCostCommission { get; set; }
        public Decimal SupplierCostTotal { get; set; }
        public Guid TargetAudiences { get; set; }
        public Guid ClientCode { get; set; }
        public Guid BaseCurrency { get; set; }
        public Guid ForeignCurrency { get; set; }
        public Decimal CurrencyExchangeRate { get; set; }
        public int Version { get; set; }
        public string Status { get; set; }
        public string Comment { get; set; }
        public bool TrackingChanges { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid LastUpdatedBy { get; set; }
        public DateTime LastUpdatedDate { get; set; }
        public string FlightDates { get; set; }
        public Guid PurchaseTypeId { get; set; }
    }
}

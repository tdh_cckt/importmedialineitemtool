﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImportMediaLineItemTool.Models
{
    public class MediaPlan
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool Published { get; set; }
        public bool Deleted { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal BudgetAllocation { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }
        public bool TrackingChanges { get; set; }
        public int? Status { get; set; }
        public int SortOrder { get; set; }
        public Guid? CampaignId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImportMediaLineItemTool.Models
{
    public class Campaign
    {
        public Guid Id { get; set; }
        public bool Published { get; set; }
        public bool Deleted { get; set; }
        public int SortOrder { get; set; }
        public string Name { get; set; }
    }
}

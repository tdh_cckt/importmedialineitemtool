﻿using ImportMediaLineItemTool.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ImportMediaLineItemTool
{
    public class MediaPlanningDbContext : DbContext
    {
        public DbSet<MediaPlan> MediaPlan { get; set; }
        public DbSet<Campaign> Campaign { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(AppSetting.SqlServerConnectionString);
        }
    }
}

﻿[
  '{{repeat(3000)}}',
  {
    IsDeleted: false,
    Name:'Sprint name {{firstName()}} {{surname()}}',
    MediaPlanId: "619888fa-7808-42da-937d-1dc874cbaea6",
    MediaType: "5e3a91d537cf114934c7b435",
    SubMediaTypeId: "5e3a91d537cf114934c7b435",
    SupplierProductType: '{{guid()}}',
    PublisherId: '{{guid()}}',
    ChannelId: '{{guid()}}',
    Position: "",
    Color: '{{random("Red", "Blue", "Black", "Yellow")}}',
    Width: "{{Math.floor(Math.random() * 10 + 1)}}",
    Height: "{{Math.floor(Math.random() * 10 + 1)}}",
    Edition: "",
    Lang: '{{random("French", "German", "Portuguese", "Korean")}}',
    Freq: '{{random("Daily", "Weekly")}}',
    StartDate: '2020-01-01T02:25:34 -07:00',
    EndDate: '2020-01-28T02:25:34 -07:00',
    BuyingModel:'{{guid()}}',
    SpotTypeId:'{{guid()}}',
    BaseRate: "{{Math.floor(Math.random() * 10+1)}}",
    BaseCost: function() {
      return this.BaseRate * this.Width * this.Height;
    },
    ClientCostDiscount: "{{Math.floor(Math.random() * 40 + 20)}}",
    ClientCostLoading: "{{Math.floor(Math.random() * 20)}}",
    ClientCostNetCost: function() {
      var a = this.BaseCost * (1-this.ClientCostDiscount/100)*(1 + this.ClientCostLoading/100);
      return a.toFixed(2);
    },
    ClientCostOtherCost: "{{Math.floor(Math.random() * 40 + 20)}}",
    ClientCostAgencyFee: "{{Math.floor(Math.random() * 40 + 20)}}",
    ClientCostTax1: "",
    ClientCostTax2: "",
    ClientCostTotal: "{{Math.floor(Math.random() * 100 + 20)}}",
    SupplierCostDiscount: "",
    SupplierCostLoading: "",
    SupplierCostNetCost: "{{Math.floor(Math.random() * 40 + 20)}}",
    SupplierCostOtherCost: "{{Math.floor(Math.random() * 40 + 20)}}",
    SupplierCostCommission: "",
    SupplierCostTotal: "{{Math.floor(Math.random() * 100 + 20)}}",
    TargetAudiences:'{{guid()}}', 
    ClientCode:'{{guid()}}',
    BaseCurrency:'{{guid()}}',
    ForeignCurrency:'{{guid()}}',
    CurrencyExchangeRate: "{{Math.floor(Math.random() * 40 + 20)}}",
    Version: 1,
    Status: "New",
    TrackingChanges: true,
    Comment: "",
    CreatedBy:'{{guid()}}',
    CreatedDate: function() {
    	return this.StartDate;
    },
    LastUpdatedBy: function() {
    	return this.CreatedBy;
    },
    LastUpdatedDate:  function() {
    	return this.StartDate;
    }
  }
]
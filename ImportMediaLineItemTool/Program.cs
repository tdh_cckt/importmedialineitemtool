﻿using ImportMediaLineItemTool.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace ImportMediaLineItemTool
{
    class Program
    {
        static void Main(string[] args)
        {
            #region import json data file
            //var client = new MongoClient("mongodb://localhost:27017");
            //var database = client.GetDatabase("Merlin");
            //var tvMediaLineItemCollection = database.GetCollection<TVMediaLineItem>("MediaLineItem");
            //var printMediaLineItemCollection = database.GetCollection<PrintMediaLineItem>("MediaLineItem");

            //var rawDataTV = System.IO.File.ReadAllText("TVMediaLineItem.txt");
            //var rawDataPrint = System.IO.File.ReadAllText("PrintMediaLineItem.txt");

            //var mediaLineItemTV = JsonConvert.DeserializeObject<List<TVMediaLineItem>>(rawDataTV);
            //var mediaLineItemPrint = JsonConvert.DeserializeObject<List<PrintMediaLineItem>>(rawDataPrint);

            //tvMediaLineItemCollection.InsertMany(mediaLineItemTV);
            //printMediaLineItemCollection.InsertMany(mediaLineItemPrint);
            #endregion

            var watch = new System.Diagnostics.Stopwatch();

            watch.Start();

            ImportListMediaLineItems();

            watch.Stop();

            Console.WriteLine($"Execution Time: {watch.ElapsedMilliseconds} ms");

            Console.WriteLine("Import Success");
        }
        public static Guid ImportToSqlServer()
        {
            var context = new MediaPlanningDbContext();
            var campaign = new Campaign()
            {
                Id = Guid.NewGuid(),
                SortOrder = 0,
                Deleted = false,
                Name = "Campaign Name Migrator",
                Published = true
            };
            var mediaPlan = new MediaPlan()
            {
                Id = Guid.NewGuid(),
                CampaignId = campaign.Id,
                BudgetAllocation = 10,
                Comment = "Comment",
                Description = "Description",
                StartDate = DateTime.UtcNow,
                EndDate = DateTime.UtcNow.AddDays(100),
                Name = "MediaPlan name",
                TrackingChanges = true,
                Deleted = false,
                Published = true,
                SortOrder = 0
            };
            
            context.Campaign.Add(campaign);
            context.MediaPlan.Add(mediaPlan);
            context.SaveChanges();
            return mediaPlan.Id;
        }
        public static void ImportListMediaLineItems()
        {
            #region declare
            var client = new MongoClient(AppSetting.MongoDbConnectionString);

            var database = client.GetDatabase(AppSetting.MongoDbDatabaseName);

            var mediaLineItemCollection = database.GetCollection<BsonDocument>(AppSetting.MediaLineItemCollectionName);

            var mediaLineItemList = new List<BsonDocument>();

            var random = new Random();

            var mediaPlanId = ImportToSqlServer();

            string[] colors = new string[] {"Red", "Blue", "Black" };
            string[] languages = new string[] { "French", "German", "Portuguese", "Korean" };
            string[] freqs = new string[] { "Daily", "Weekly" };
            string[] weekdays = new string[] { "Monday, Tuesday", "Monday, Tuesday, Wednesday", "Monday, Tuesday, Wednesday,Thursday", "Sunday" };
            string[] status = new string[] { "New", "Confirmed", "Pending approval", "Approved" };
            string[] adServingTypes = new string[] { "Video", "Text", "Search", "Banner" };

            string tvMediaType = GetMediaTypeId("TV Content");
            string printMediaType = GetMediaTypeId("Print Content");
            string digitalMediaType = GetMediaTypeId("Digital");
            #endregion
            for (int i=0; i<1000; i++)
            {
                #region random data
                var colorIndex = random.Next(0, colors.Length);
                var languageIndex = random.Next(0, languages.Length);
                var width = random.Next(1,10);
                var height = random.Next(1, 10);
                var freqIndex = random.Next(0, freqs.Length);
                var baseRate = random.Next(1, 10);
                var weekdayIndex = random.Next(1, weekdays.Length);
                var statusIndex = random.Next(1, weekdays.Length);
                var numberOfSpot = random.Next(1, 5);
                var adServingTypeIndex = random.Next(0, adServingTypes.Length);
                #endregion
                var print = new PrintMediaLineItem()
                {
                    #region print data
                    Id = ObjectId.GenerateNewId(),
                    IsDeleted = false,
                    Name = "Print: " + i.ToString(),
                    MediaPlanId = mediaPlanId,
                    MediaTypeId = printMediaType,
                    SubMediaTypeId = "",
                    SupplierProductId = Guid.NewGuid(),
                    SupplierId = Guid.NewGuid(),
                    SubProductId = Guid.NewGuid(),
                    Position = "",
                    Color = colors[colorIndex],
                    Width = width,
                    Height = height,
                    Edition = "",
                    Language = languages[languageIndex],
                    Frequency = freqs[freqIndex],
                    StartDate = DateTime.UtcNow,
                    EndDate = DateTime.UtcNow.AddDays(i + 1),
                    BuyingMethodId = Guid.NewGuid(),
                    SpotTypeId = Guid.NewGuid(),
                    BaseRate = baseRate,
                    BaseCost = baseRate * width * height,
                    ClientCostDiscount = 0,
                    ClientCostLoading = 0,
                    ClientCostNetCost = baseRate * width * height,
                    ClientCostOtherCost = random.Next(1, 99),
                    ClientCostAgencyFee = random.Next(1, 99),
                    ClientCostTax1 = random.Next(1, 99),
                    ClientCostTax2 = random.Next(1, 99),
                    ClientCostTotal = random.Next(1, 99),
                    SupplierCostDiscount = random.Next(1, 99),
                    SupplierCostLoading = random.Next(1, 99),
                    SupplierCostNetCost = random.Next(1, 99),
                    SupplierCostOtherCost = random.Next(1, 99),
                    SupplierCostCommission = random.Next(1, 99),
                    SupplierCostTotal = random.Next(1, 99),
                    TargetAudiences = Guid.NewGuid(),
                    ClientCode = Guid.NewGuid(),
                    BaseCurrency = Guid.NewGuid(),
                    ForeignCurrency = Guid.NewGuid(),
                    CurrencyExchangeRate = random.Next(1, 99),
                    Version = 1,
                    Status = "New",
                    TrackingChanges = true,
                    Comment = "",
                    CreatedBy = Guid.NewGuid(),
                    CreatedDate = DateTime.UtcNow,
                    LastUpdatedBy = Guid.Empty,
                    LastUpdatedDate = DateTime.UtcNow,
                    PublisherId = Guid.NewGuid(),
                    PurchaseTypeId = Guid.NewGuid(),
                    FlightDates = DateTime.UtcNow.ToShortDateString() + "-" + DateTime.UtcNow.AddDays(i + 1).ToShortDateString(),
                    #endregion
                }.ToBsonDocument();

                var tv = new TVMediaLineItem()
                {
                    #region tv data
                    Id = ObjectId.GenerateNewId(),
                    IsDeleted = false,
                    Name = "TV: " + i.ToString(),
                    MediaPlanId = mediaPlanId,
                    MediaTypeId = tvMediaType,
                    SubMediaTypeId = "",
                    SupplierProductId = Guid.NewGuid(),
                    SupProductId = Guid.NewGuid(),
                    SupplierId = Guid.NewGuid(),
                    StartDate = DateTime.UtcNow,
                    EndDate = DateTime.UtcNow.AddDays(i + 1),
                    Weekday = weekdays[weekdayIndex],
                    Daypart = weekdays[weekdayIndex],
                    BuyingMethodId = Guid.NewGuid(),
                    Volume = numberOfSpot,
                    Grp = random.Next(1, 100),
                    SpotTypeId = Guid.NewGuid(),
                    BaseRate = baseRate,
                    BaseCost = numberOfSpot * baseRate,
                    ClientCostDiscount = random.Next(1, 100),
                    ClientCostLoading = random.Next(1, 100),
                    ClientCostNetCost = random.Next(1, 100),
                    ClientCostOtherCost = random.Next(1, 100),
                    ClientCostAgencyFee = random.Next(1, 100),
                    ClientCostTax1 = random.Next(1, 100),
                    ClientCostTax2 = random.Next(1, 100),
                    ClientCostTotal = random.Next(1, 100),
                    SupplierCostDiscount = random.Next(1, 100),
                    SupplierCostNetCost = random.Next(1, 100),
                    SupplierCostCommission = random.Next(1, 100),
                    SupplierCostLoading = random.Next(1, 100),
                    SupplierCostOtherCost = random.Next(1, 100),
                    SupplierCostTotal = random.Next(1, 100),
                    CurrencyExchangeRate = random.Next(1, 100),
                    TargetAudiences = Guid.NewGuid(),
                    BaseCurrency = Guid.NewGuid(),
                    ForeignCurrency = Guid.NewGuid(),
                    ClientCode = Guid.NewGuid(),
                    Version = 1,
                    Status = status[statusIndex],
                    Comment = "",
                    CreatedBy = Guid.NewGuid(),
                    CreatedDate = DateTime.UtcNow,
                    LastUpdatedBy = Guid.Empty,
                    LastUpdatedDate = DateTime.UtcNow,
                    TrackingChanges = true,
                    FlightDates = DateTime.UtcNow.ToShortDateString() + "-" + DateTime.UtcNow.AddDays(i + 1).ToShortDateString(),
                    PurchaseTypeId = Guid.NewGuid()
#endregion
                }.ToBsonDocument();

                var digital = new DigitalMediaLineItem()
                {
                    #region digital data
                    Id = ObjectId.GenerateNewId(),
                    IsDeleted = false,
                    Name = "Digital: " + i.ToString(),
                    MediaPlanId = mediaPlanId,
                    MediaTypeId = digitalMediaType,
                    SubMediaTypeId = "",
                    SupplierProductId = Guid.NewGuid(),
                    PublisherId = Guid.NewGuid(),
                    Format = "advertisement together with WidthxHeight",
                    Width = width,
                    Height = height,
                    LandingPage = "A website name",
                    AdServingType = adServingTypes[adServingTypeIndex],
                    StartDate = DateTime.UtcNow,
                    EndDate = DateTime.UtcNow.AddDays(i + 1),
                    BuyingModel = Guid.NewGuid(),
                    Volume = random.Next(1, 60),
                    SpotTypeId = Guid.NewGuid(),
                    BaseRate = baseRate,
                    BaseCost = random.Next(1, 100),
                    ClientCostDiscount = random.Next(1, 100),
                    ClientCostAgencyFee = random.Next(1, 100),
                    ClientCostLoading = random.Next(1, 100),
                    CurrencyExchangeRate = random.Next(1, 100),
                    ClientCostNetCost = random.Next(1, 100),
                    ClientCostOtherCost = random.Next(1, 100),
                    ClientCostTax1 = random.Next(1, 100),
                    ClientCostTax2 = random.Next(1, 100),
                    ClientCostTotal = random.Next(1, 100),
                    BaseCurrency = Guid.NewGuid(),
                    CreatedBy = Guid.NewGuid(),
                    LastUpdatedBy = Guid.Empty,
                    ClientCode = Guid.NewGuid(),
                    Comment = "",
                    ForeignCurrency = Guid.NewGuid(),
                    CreatedDate = DateTime.UtcNow,
                    LastUpdatedDate = DateTime.UtcNow,
                    Status = status[statusIndex],
                    SupplierCostCommission = random.Next(1, 100),
                    SupplierCostDiscount = random.Next(1, 100),
                    SupplierCostLoading = random.Next(1, 100),
                    SupplierCostNetCost = random.Next(1, 100),
                    SupplierCostOtherCost = random.Next(1, 100),
                    SupplierCostTotal = random.Next(1, 100),
                    TargetAudiences = Guid.NewGuid(),
                    TrackingChanges = true,
                    Version = 1,
                    PurchaseTypeId = Guid.NewGuid(),
                    SupplierId = Guid.NewGuid(),
                    FlightDates = DateTime.UtcNow.ToShortDateString() + "-" + DateTime.UtcNow.AddDays(i + 1).ToShortDateString()
                    #endregion
                }.ToBsonDocument();
                mediaLineItemList.Add(print);
                mediaLineItemList.Add(tv);
                mediaLineItemList.Add(digital);
            }
            mediaLineItemCollection.InsertMany(mediaLineItemList);
        }

        public static string GetMediaTypeId(string mediaTypeName)
        {
            var client = new MongoClient(AppSetting.MongoDbConnectionString);

            var database = client.GetDatabase(AppSetting.MongoDbDatabaseName);

            var mediaTypeItemCollection = database.GetCollection<MediaType>(AppSetting.MediaTypeItemCollectionName);

            var mediaType = mediaTypeItemCollection.Find<MediaType>(mediaType => mediaType.Name == mediaTypeName).FirstOrDefault();

            return mediaType.Id;
        }
    }
}
